# ILVO pipeline for Virus Detection in RNA-seq
## 0. Developers
[Annelies Haegeman](mailto:annelies.haegeman@ilvo.vlaanderen.be) and [Yoika Foucart](mailto:yoika.foucart@ilvo.vlaanderen.be)
Flanders Research Institute for Agriculture, Fisheries and Food ([ILVO](http://www.ilvo.vlaanderen.be))

## 1. Introduction
This pipeline is used for Illumina total RNA-seq data (2x150 bp).
The pipeline can be run on a local Linux system or server, where the following software is installed and available in $PATH:
- [FastQC](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
- [Cutadapt (Martin,2011)](https://cutadapt.readthedocs.io/en/stable/)
- [Pear (Zhang et al., 2014)](http://www.exelixis-lab.org/web/software/pear)
- [SortMeRNA (Kopylova et al., 2012)](https://bioinfo.lifl.fr/RNA/sortmerna/)
- [VirusDetect (Zheng et al., 2016)](http://virusdetect.feilab.net/cgi-bin/virusdetect/index.cgi)

The pipeline consists of two parts: a preprocessing part, and a VirusDetect part. In case you have multiple samples, you can also use a shell script which can do all preprocessing steps at once for multiple samples.

## 2. Preprocessing of data

In the following sections, the different preprocessing steps are explained.

In case you want to run all the steps automatically and on multiple sample, you can use a shell script. An [example shell script](https://gitlab.com/ilvo/phbn-wp2-training/blob/master/CL_ILVO_1/scripts/virus_RNAseq_preprocessing.sh) is available in the scripts folder. Be aware that you will need to do some custom adjustments in case you want to use that shell script (file path to sortmerna database, path to folder containing samples to be analyzed, suffix of the files to be analyzed, etc.). The shell script also outputs a text file with the number of reads which are retained after each processing step. There is also an [R script](https://gitlab.com/ilvo/phbn-wp2-training/blob/master/CL_ILVO_1/scripts/Preprocessing_statistics.Rmd) available to visualize the number of reads in barplots.

If it is the first time you run this pipeline, it is recommended to use the seperate steps as explained below.

### 2.1 QC

First, unzip your file using gunzip:
`gunzip .fastq.gz`
If you have more files:
`gunzip *.fastq.gz`

To make a quick quality report of the raw data, you can run the software FastQc as follows with your fastq file as imput:
`fastqc <raw data file in fastq>`

>*For example:* `fastqc SampleID.fastq`

### 2.2 Remove adapter sequences
Next, you need to check your reads for the presence of 'Illumina' adapter sequences.  In the FastQC report you can already see which adapters are probably present.
In order to exactly determine the adapter sequences, it is best to check your read files using head and to manually check the adapter sequences (especially the start). You can do this by comparing the corresponding read pairs and see where the overlap is.
`head forward_reads.fastq`
`head reverse_reads.fastq`

>*For example (the adapters are bold):*
Forward read GTTTGAGGCAATAACAGGTCTGTGATGCCCTTAGATGTTCTGGGCCGCACGCGCGCTACACTGATGTATTCAACGAG**AGATCGGAAGAGCACACGTCTGAACTCCAGTCACTACGCTGCATCTCGTATGCCGTCTTCTGCTTGAAAAAAT**   Reverse read CTCGTTGAATACATCAGTGTAGCGCGCGTGCGGCCCAGAACATCTAAGGGCATCACAGACCTGTTATTGCCTCAAAC**AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTACTCTAGGGTGTAGATCTCGGTGGTCGCCGTATCATTAAA**

Using Cutadapt you can remove the adapters from all your sequences.

`cutadapt -a <3' end adapter> -A <5' end adapter> -o <forward trimmed output fastq filename> -p <reverse trimmed output fastq filename> <forward reads in fastq> <reverse reads in fastq>`

The following options can be used:
`-a`: the 3’ end adapter of the forward reads
`-A` : the 3’ end adapter of the reverse reads
`-e` : the maximum error rate
`-j` : numbers of threads/processors to use
`-o` : the output file name for the trimmed forward reads
`-p` : the output file name for the trimmed reverse reads
`-m` : the minimum length the resulting trimmed fragment should have
`-M` : the maximum length the resulting trimmed fragment should have

>*For example:*
`cutadapt -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCACTACGCT -A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTACTCTAG -e 0.10 -j 8 -o SampleID_F_trimmed.fastq -p SampleID_R_trimmed.fastq Forward_reads.fastq Reverse_reads.fastq -m 30`

The program allows a maximum of 10% errors compared to the adapter sequence. This means 0 errors are allowed in fragments from 0-9 bp, 1 error in fragments of 10-19 bp, 2 errors of fragments of 20-29 bp … . In this case we also remove trimmed fragments that are smaller than 30 bp. The protocol is run on 4 processors.

### 2.3 Merging F and R reads
This step merges the forward and reverse read using the program Pear. The program adjusts the quality scores in the overlapping zone: if the two reads have the same base in the overlapping region, then the quality score will go up, while if the base is different, the base with the largest quality score will be chosen, with its corresponding quality score.

`pear -f <forward trimmed fastq file>' -r <reverse trimmed fastq file> -o <samplename>`

The following options can be used:
`-o` : output file name base
`-p` : p-value testing disabled
`-v` : minimum overlap size
`-n` : minimum possible length
`-m` : maximum possible length
`-y` : Specify the amount of memory to be used. The number may be followed by one of the letters K, M, or G denoting Kilobytes, Megabytes and Gigabytes, respectively
`-j` : number of threads/processors to use
`-q` : quality threshold: if two consecutive bases are below this threshold, the read is trimmed
`-t` : minimum length after trimming
`-u` : discard reads with more than 10% N's

>*For example:*
`pear -f SampleID_F_trimmed.fastq -r SampleID_R_trimmed.fastq -o SampleID -v 20 -y 100G -j 8 -q 20 -t 30 -u 0.1`

The program merges the forward and reverse read with a minimum overlap size of 20bp and a minimum trimming length of 30bp, reads with more than 10% N's will be discarded.
The program is run on 4 processors and uses 100 Gigabytes memory. If two consecutive bases are below 20, the read is trimmed.

### 2.4 Remove rRNA sequences
In this step, rRNA sequences can be removed. In this case, we use the software “sortmerna”, which tries to find matches to the different rRNA databases of SILVA.

#### 2.4.1 Merged reads
For the merged reads, we only have 1 input file, i.e. the “.assembled” file resulting from the previous step. We can now sort the sequences into rRNA and non rRNA sequences with the following command:
`sortmerna --ref /path/to/sortmerna/databases --reads <assembled reads fastq> --fastx --aligned <trimmed and filterend rRNA output fastq> --other <trimmed and filtered non rRNA output fastq>`

The following options can be used:
`--ref` : reference rRNA databases the reads need to be compared to, with first the reference file (.fasta) and then the corresponding index file (.idx) separated by a comma; if multiple references are used, separate them with a :
`--reads` : input file of reads
`--sam` : output sam alignment for aligned reads (not necessary in this case)
`--num_alignments` : report only the first x   alignments of a read (not necessary in this case)
`--fastx` : output fasta or fastq file for aligned and non-aligned reads
`--aligned` : file with rRNA reads that were able to align to one of the reference databases
`--other` : file with non rRNA reads
`-a` : number of processors to use
`--log` : output some statistics
`-v` : verbose

>*For example:*
`sortmerna --ref /usr/share/bioinf_databases/sortmerna/silva-bac-16s-id90.fasta,/usr/share/bioinf_databases/sortmerna/silva-bac-16s-id90.idx:/usr/share/bioinf_databases/sortmerna/silva-bac-23s-id98.fasta,/usr/share/bioinf_databases/sortmerna/silva-bac-23s-id98.idx:/usr/share/bioinf_databases/sortmerna/silva-arc-16s-id95.fasta,/usr/share/bioinf_databases/sortmerna/silva-arc-16s-id95.idx:/usr/share/bioinf_databases/sortmerna/silva-arc-23s-id98.fasta,/usr/share/bioinf_databases/sortmerna/silva-arc-23s-id98.idx:/usr/share/bioinf_databases/sortmerna/silva-euk-18s-id95.fasta,/usr/share/bioinf_databases/sortmerna/silva-euk-18s-id95.idx:/usr/share/bioinf_databases/sortmerna/silva-euk-28s-id98.fasta,/usr/share/bioinf_databases/sortmerna/silva-euk-28s-id98.idx:/usr/share/bioinf_databases/sortmerna/rfam-5s-database-id98.fasta,/usr/share/bioinf_databases/sortmerna/rfam-5s-database-id98.idx:/usr/share/bioinf_databases/sortmerna/rfam-5.8s-database-id98.fasta,/usr/share/bioinf_databases/sortmerna/rfam-5.8s-database-id98.idx
--reads SampleID.assembled.fastq --fastx --aligned SampleID_rRNA --other SampleID_non_rRNA -a 8 --log -v`

#### 2.4.2 Non-merged reads (remaining pairs)
This step can be skipped since in most cases the library insert size is relatively low, and most reads make the overlap in the merging step.  In case __the non assembled percentage is >30%__ you can include the non-merged or non-assembled reads in the data analysis as well.

For the non-merged reads, we have 2 input files, i.e. the foward unassembled and the reverse unassembled file from the previous step. SortmeRNA can only take 1 file as input file. Therefore we first need to convert the two paired fastq files to 1 “interleaved” fastq file, where both corresponding reads are directly after each other in the file. Converting two paired files to one interleaved file can be done as follows:

`bash /usr/share/sortmerna/scripts/merge-paired-reads.sh forward-reads.fastq reverse-reads.fastq outputfile-interleaved.fastq`
The bash script “merge-paired-reads.sh” from the SortmeRNA software takes as first and second argument the two paired fastq files and as third argument the name you choose for the resulting interleaved output file.

Next the same command as for the merged reads can be used to look for rRNAs, but you need to use the interleaved fastq file as input file, and you also need to add the “--paired-in” option. This option specifies that if 1 of both read pairs matches with rRNA, both read pairs will go into the rRNA file.
`sortmerna --ref /path/to/sortmerna/databases --fastx -–paired_in --aligned reads-interleaved_rRNA --other reads-interleaved_non_rRNA -a 8 --log -v`

Finally, you can reconvert the resulting interleaved reads again into two separate files with the bash script **“unmerge-paired-reads.sh”**, used in a similar way as the previous bash script, now with the interleaved file as first argument and the names you choose as output file for both paired reads files.

`bash /usr/share/sortmerna/scripts/unmerge-paired-reads.sh reads-interleaved_non_rRNA.fastq <forward unassembled fastq file > <reverse unassembled fastq file>`

>*For example:*
`bash /usr/share/sortmerna/scripts/unmerge-paired-reads.sh interleaved_non_rRNA.fastq SampleID.unassembled.forward.fastq SampleID.unassembled.reverse.fastq`

## 3. VirusDetect Pipeline
In the next steps, the VirusDetect pipeline is followed. VirusDetect is a software package that can efficiently and exhaustively analyze large-scale sRNA or RNA-seq datasets for virus identification. The program performs reference-guided assembly by aligning sRNA reads to the known virus reference database (GenBank gbvrl) as well as de novo assembly using Velvet with automated parameter optimization. The assembled contigs are compared to the reference virus sequences for virus identification.

For more information about the pipeline, see *http://bioinfo.bti.cornell.edu/tool/VirusDetect/*.   In the case of RNA-seq data, it uses the program “hisat” to map the reads to the reference genome, although it is not clear from the manual how the software “knows” that you are dealing with RNA-seq data and not miRNA data.

### 3.1	Reference databases
#### 3.1.1	Virus databases
To run the VirusDetect pipeline, we need different reference databases. These reference databases are stored in a central folder on the server where VirusDetect is installed: */usr/local/bin/VirusDetect/databases*. The software comes with the dataset “vrl_Plants” and “vrl_Plants_prot”. These are viruses derived from plants, the database was made by the Virus Classification Pipeline (*https://github.com/kentnf/VirusDetect/tree/master/tools/genbank_vrl_classification*). On the FTP site of the software (*ftp://bioinfo.bti.cornell.edu/pub/program/VirusDetect/virus_database/*), you can download additional databases, for example a database of viruses from invertebrates. In this case you can choose different thresholds at which redundant sequences were removed (100%: all sequences retained in database, unless there is an exact match; 97%: only 1 sequence is retained of all sequences that have a minimum identity of 97% of each other; same with 95%). The databases need to be indexed for use with bwa and with blast, but in the downloadable databases, this has already been done.
You can also make your own databases and add these to the databases folder. These databases still need to be index for bwa (using bwa index) and for blast (using formatdb). For more information, see the VirusDetect website.

#### 3.1.2	Host databases
You can also include host databases, these will be used for mapping the reads; mapped reads are derived from the host and not from the virus and will be removed from the analysis. Try to find the a genome of a plant as closely as possible related to your plant. Use “NCBI Taxonomy” to click on your organism and to see if there are genomes available (in the right column with databases, look for the “Genome” database). If no genome is available for your plant, select the corresponding genus or family of the plant in NCBI Taxonomy, and again check if you can find a “Genome” entry.

The host databases need to be added to the databases folder where VirusDetect is installed: */path/to/VirusDetect/databases*. For example the downloaded pear genome “*Pyrus_communis_v1.0-scaffolds.fna*” is added to the databases folder. Next, these host genomes need to be indexed for the mapping programs [BWA](http://bio-bwa.sourceforge.net/) and HISAT2(https://ccb.jhu.edu/software/hisat2/index.shtml) as follows:

`bwa index /path/to/VirusDetect/databases/genome.fna`
`hisat2-build /path/to/VirusDetect/databases/genome.fna genome.fna`

>*For example:*
`cd /usr/local/bin/VirusDetect/databases`
`bwa index Pyrus_communis_v1.0-scaffolds.fna`
`hisat2-build Pyrus_communis_v1.0-scaffolds.fna Pyrus_communis_v1.0-scaffolds.fna`

#### 3.1.3	Combining merged and non-merged reads into 1 file
Since the VirusDetect pipeline cannot handle paired files, but only accepts 1 input file containing all data, it is best to combine all merged reads with the non-merged reads into 1 file.
This means we lose the paired end information of the remaining read pairs. You can combine all data simply by using the Linux command `cat.`
`cat reads.assembled.fastq forward-reads.fastq reverse-reads.fastq > all_reads.fastq`

#### 3.1.4	Running the VirusDetect pipeline
Next, you can run the VirusDetect pipeline as follows:
`virus_detect.pl <non rRNA read fastq file>`

The script takes as argument(s) the input file(s) either in fastq or fasta format. You can specify more than 1 input file, these will then be processed sequentially.

The following options can be used:
--reference : name of the reference virus database, in this case “vrl_Plants_232_U100” or “vrl_Invertebrates_232_U100”
--host_reference : name of the host reference database used for host sequence subtraction, in this case   “GCF_000148765.1_Malus_domestica_1.0_genomic.fna” for apple, “Pyrus_communis_v1.0-scaffolds.fna” for pear and “GCF_000346465.1_Prunus_persica_1_0_genomic.fna” for cherry (reference genome of peach, closest relative of cherry which had its genome sequenced).
--thread_num : number of processors used for alignments

Additional parameters can be set for each step (bwa alignment, hisat, blast, filtering of results).

 These options are explained well on the website *http://bioinfo.bti.cornell.edu/tool/VirusDetect/ .* The website also give more information about the output files and their format.
>*For example:*
`virus_detect.pl --reference vrl_Plants_239_U100 --host_reference GCF_000148765.1_Malus_domestica_1.0_genomic.fna --thread_num 8 SampleID_non_rRNA.fastq`
