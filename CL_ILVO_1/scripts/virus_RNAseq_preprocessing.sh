#!/bin/bash
# Annelies Haegeman 2018
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script can be used to preprocess RNA-seq data generated for the virus detection
# The steps done are quality check, adapter removal, merging of forward and reverse reads and rRNA sorting
# After the preprocessing, the resulting reads can be used for the Virusdetect pipeline.

#Specify here the folder where your samples are (in fastq.gz format):
DIR=/home/genomics/ahaegeman/Virus/preprocessing_script_RNAseq
#Specify here the filepath to the FastQ_CountReads.py script
PYTHONSCRIPT="/home/genomics/kdejonghe/PROTOCOLS_LINUX_totalRNA-smallRNA/FastQ_CountReads.py"

date
echo

# Change directory to specified folder
cd $DIR
# Initiate file with number of reads
echo -e "sample""\t""raw read pairs""\t""trimmed read pairs""\t""merged reads""\t""non rRNA merged reads" >> Number_of_reads.txt

# Define the variable FILES that contains all the forward data sets (here only forward, otherwise you will do everything in duplicate!)
# When you make a variable, you can not use spaces! Otherwise you get an error.
FILES=( *_R1_001.fastq.gz )

#Loop over all files and do all the commands
for f in "${FILES[@]}" 
do 
	#Define the variable SAMPLE who contains the basename where the extension is removed (-1.fastq.gz)
	SAMPLE=`basename $f _R1_001.fastq.gz`

	echo "PROCESSING $SAMPLE...."
	echo

	#MAKE DIRECTORY FOR THE SAMPLE
	mkdir $SAMPLE

	#QUALITY CHECK
	echo "Checking quality for $SAMPLE...."
	fastqc "$SAMPLE"_R1_001.fastq.gz -o $SAMPLE
	fastqc "$SAMPLE"_R2_001.fastq.gz -o $SAMPLE
	echo "Done checking quality for $SAMPLE."
	echo

	#ADAPTER REMOVAL
	echo "Removing adapters for $SAMPLE...."
	cutadapt -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCACTACGCT -A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTACTCTAG -o ./$SAMPLE/"$SAMPLE"_F_trimmed.fastq -p ./$SAMPLE/"$SAMPLE"_R_trimmed.fastq "$SAMPLE"_R1_001.fastq.gz "$SAMPLE"_R2_001.fastq.gz -m 30
	echo "Done removing adapters for $SAMPLE."
	echo

	#MERGING
	echo "Merging F and R reads for $SAMPLE...."
	pear -f ./$SAMPLE/"$SAMPLE"_F_trimmed.fastq -r ./$SAMPLE/"$SAMPLE"_R_trimmed.fastq -o ./$SAMPLE/"$SAMPLE" -p 1.0 -v 20 -y 100G -j 4 -q 20 -t 30 -u 0.1
	echo "Done merging F and R reads $SAMPLE...."
	echo

	#rRNA REMOVAL
	echo "Removing rRNA for $SAMPLE...."
	sortmerna --ref /usr/local/bioinf/sortmerna/rRNA_databases/silva-bac-16s-id90.fasta,/usr/local/bioinf/sortmerna/rRNA_databases/silva-bac-16s-id90.idx:/usr/local/bioinf/sortmerna/rRNA_databases/silva-bac-23s-id98.fasta,/usr/local/bioinf/sortmerna/rRNA_databases/silva-bac-23s-id98.idx:/usr/local/bioinf/sortmerna/rRNA_databases/silva-arc-16s-id95.fasta,/usr/local/bioinf/sortmerna/rRNA_databases/silva-arc-16s-id95.idx:/usr/local/bioinf/sortmerna/rRNA_databases/silva-arc-23s-id98.fasta,/usr/local/bioinf/sortmerna/rRNA_databases/silva-arc-23s-id98.idx:/usr/local/bioinf/sortmerna/rRNA_databases/silva-euk-18s-id95.fasta,/usr/local/bioinf/sortmerna/rRNA_databases/silva-euk-18s-id95.idx:/usr/local/bioinf/sortmerna/rRNA_databases/silva-euk-28s-id98.fasta,/usr/local/bioinf/sortmerna/rRNA_databases/silva-euk-28s-id98.idx:/usr/local/bioinf/sortmerna/rRNA_databases/rfam-5s-database-id98.fasta,/usr/local/bioinf/sortmerna/rRNA_databases/rfam-5s-database-id98.idx:/usr/local/bioinf/sortmerna/rRNA_databases/rfam-5.8s-database-id98.fasta,/usr/local/bioinf/sortmerna/rRNA_databases/rfam-5.8s-database-id98.idx --reads ./$SAMPLE/$SAMPLE.assembled.fastq --fastx --aligned ./$SAMPLE/"$SAMPLE"_rRNA --other ./$SAMPLE/"$SAMPLE"_non_rRNA -a 8 --log -v
	echo "Done removing rRNA for $SAMPLE."
	echo

	#COUNT NUMBER OF READS
	RAWREADS=`zcat "$SAMPLE"_R1_001.fastq.gz | wc -l |awk '{print $1/4}'`
	TRIMMEDREADS=`python $PYTHONSCRIPT -i ./$SAMPLE/"$SAMPLE"_F_trimmed.fastq`
	MERGEDREADS=`python $PYTHONSCRIPT -i ./$SAMPLE/"$SAMPLE".assembled.fastq`
	NONRRNAREADS=`python $PYTHONSCRIPT -i ./$SAMPLE/"$SAMPLE"_non_rRNA.fastq`
	echo -e "$SAMPLE""\t""$RAWREADS""\t""$TRIMMEDREADS""\t""$MERGEDREADS""\t""$NONRRNAREADS" >> Number_of_reads.txt

	echo "DONE PROCESSING $SAMPLE."
	echo
	echo

done

date