# NIB pipeline for Virus Detection in RNA-seq
## 0. Developers
[Denis Kutnjak](mailto:Denis.Kutnjak@nib.si), [Anja Pecman](mailto:Anja.Pecman@nib.si), Olivera Maksimović Carvalho Ferreira, Maja Ravnikar
National Institute of Biology (Slovenia) ([NIB](https://www.nib.si/eng/))

If using the pipeline, please cite [this publication](https://doi.org/10.3389/fmicb.2017.01998), which contains the description of the pipeline.

## 1. Pipeline
This pipeline runs with the commercial software [CLC Genomics Workbench](https://www.qiagenbioinformatics.com/products/clc-genomics-workbench/).

The workflow which can be imported in CLC Genomics Workbench can be downloaded [here](https://gitlab.com/ilvo/phbn-wp2-training/blob/master/CLC_NIB_1/totRNA_Diagnostics_v5.2_Sep_19.cpw).

A manual in pdf format can be downloaded [here](https://gitlab.com/ilvo/phbn-wp2-training/blob/master/CLC_NIB_1/CLC_pipeline_PHBN.pdf).
