# Pipeline for virus/viroid detection using Geneious Prime

## 0. Developer
[Paolo Margaria](mailto:paolo.margaria@dsmz.de), Leibniz Institute DSMZ-German Collection of Microorganisms and Cell Cultures GmbH, Inhoffenstraße 7B, 38124 Braunschweig, Germany

## 1. Introduction
The pipeline is used on Illumina total RNA-seq data, obtained through NextSeq (2X150 bp) or MiSeq (2X300 bp) platforms, with the purpose to identify viral/viroid sequences through BLASTn analysis of assembled contigs against a reference database. The [Geneious Prime](www.geneious.com) software is a licensed bioinformatics software developed by Biomatters. The pipeline can be run step by step or alternatively as a convenient workflow (attached file), to complete all steps automatically and sequentially for multiple samples. Note that complementary approaches and further analyses might be necessary for fine mapping, reconstruction of complete genomes or in-depth virus discovery (for example, through BLASTp/protein domain search).

## 2. Pre-processing of the fastq raw reads

### 2.1 Quality control
A quick quality report of the raw data can be obtained using the FastQC software. Download and install the software ([FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)), choose the fastq read files as input and run the analysis; you obtain a general report that can be used to quickly visually check the quality of the data before analysis.

### 2.2 Import/pair reads into Geneious
Import data into Geneious by dragging and dropping the files into the software’s main window. For paired read data, the forward and reverse read lists in fastq format can be paired in a single file. Geneious is able to guess the read technology used and asks for the expected insert size (i.e. the expected average insert size excluding adapters; this specification is not critical for the analysis, but rather related to graphical visualization of the data).

![QC_1](./images/QC_1.png)

You can now proceed with the analysis step by step as described below, or run the Workflow presented in section 5.

### 2.3 Trimming
Be sure that the BBDuk trimmer plugin is installed (Tools=>Plugins). You can then proceed to trim adapters by selecting: the pre-set Illumina adapters list, trim end by quality, trim adapters based on overhangs and discard short reads. Go to:

*Annotate&Predict =>Trim using BBDuk*

Options:

*Trim Adapters: Adapters: select appropriate adapters list; Trim: Replace with N*

*Trim Low Quality: Trim: Both Ends, Minimum Quality: 20 (Phred score)*

*Trim adapters based on paired read overhangs: Minimum overlap: 20
Discard Short Reads: Minimum Length: 50 bp*

![Trimming_1](./images/Trimming_1.png)

**Output**: the operation will generate a list of trimmed reads. You can check how the reads were trimmed by looking into the Sequence View and Lenghts Graph tabs above the viewer.

![Trimming_2](./images/Trimming_2.png)

## 3. Subtracting reads mapping the host genome
Mapping the reads against host sequences allows to reduce the number of reads used in the further steps. This procedure significantly reduces the computational requirements and time necessary to complete the following operations and also results in improved assembly accuracy.
### 3.1 Download the nuclear, mitochondrial and chloroplast genome reference sequences of the host.
Sequences can be searched in NCBI and downloaded directly through Geneious. Go to the left Menu,
*NCBI => Genome, and search host by botanic name, check RefSeq only option=>Select the sequence(s) of interest=>Download sequence(s) into a new folder, for example “Host_Sequences”.*

![Mapping_1](./images/Mapping_1.png)

Alternatively, a file of references sequences can be generated independently and imported into Geneious.

### 3.2 Map reads to host reference sequences
Now you can map the trimmed reads against the host. Select the created trimmed reads file, then click on :
*Align/Assemble=>Map to Reference*. A windows pops up, with four main fields.

*Data: Reference Sequence*: select appropriate file

*Method: Mapper: Geneious*
	      *Sensitivity: Medium-Low sensitivity* (it is a good start)
	      *Fine tuning: None*

*Trim before mapping: Do not trim*

*Results*: check “*Save assembly Report*”; “*Save list of unused reads*”; “*Save in sub-folder*”

By clicking on *More Options*, the *Advanced* window appears, showing the parameters used for mapping. These settings can be modified, provided that “*Custom Sensitivity*” is chosen in the *Method* window.  Note as well that in *Method* a number of different mapping algorithms are available, each of them having varying strengths and weaknesses depending on the type of data used in the analysis (refer to the Geneious Prime Manual for more details).

![Mapping_2](./images/Mapping_2.png)

**Output**: three files will be generated: Assembly report, Unused Reads paired, Unused Reads unpaired. It is convenient to group the reads in a single file, through: *Sequence=>Group sequences into a list* (for example named: “Minus_Host_Reads”).

## 4. De Novo assembly
### 4.1 Build contigs
Though this operation, Geneious will build a list of contigs. Select the list of Unused reads, then click in the toolbar:

*Align/Assemble=>De Novo Assemble*

A window will appear, with 5 sections (click on *More Options* to view all):

*Data*

*Method: Assembler: Geneious; Sensitivity: Low Sensitivity;* you can choose a range of sensitivities (higher sensitivity will require more memory). “Low sensitivity” is a good start.

*Trim Before Assembly: “Do not Trim”*

*Results: Save assembly report, Save in subfolder, Save contigs (Maximum 500), Save consensus sequences*

*Advanced Settings*, via the *More Options* button: this window allows to set custom values, provided that “*Custom Sensitivity*” is selected in the Method window.

![De_novo_assembly_1](./images/De_novo_assembly_1.png)

**Output**: upon completion of the assembly, three files will be generated and saved in a sub-folder: the assembly report, the consensus sequences file and the list of contigs. Select each file to see the content in detail. In the Assembly report file, a summary of the analysis is described; you can also find the *Show options* link that shows the Assembler options used during assembly. Various statistics including N50 are also reported.
By selecting the contig file, the assembled paired reads can be visualized. By default, the assembled paired reads will be colored based on the expected distance defined before (step 2.2).

![De_novo_assembly_2](./images/De_novo_assembly_2.png)

Note: other processing tools like “Normalize reads”, “Remove duplicate reads”, “Remove chimeras”, “Merge Reads” could eventually be implemented in the pipeline. Check the Geneious user manual for further details.

## 4. BLASTn analysis
A BLASTn analysis can now be performed, by using as query the assembled contigs.
### 4.1 Create BLAST database
You have first to define a BLASTn database, for example the viral/viroid RefSeq NCBI sequences. You can download it from [NCBI](https://www.ncbi.nlm.nih.gov/genomes/GenomesGroup.cgi?taxid=10239), or prepare a FASTA file of reference sequences to use as custom database. To add the database into Geneious, go to *Tools=>Add/Remove Databases=>Add Database*.

![Blastn_1](./images/Blastn_1.png)

### 4.2 Create Consensus sequences.
Click on the “*Consensus sequences*” file, and select the first 500 contigs (the higher the number of the contig, the lowest the coverage), then click *Extract*, and create a new file that will be used as query for BLAST.
### 4.3 Run BLASTn.
Select the Consensus sequence file created above, then go to:

*Tools =>BLAST*

*Query: Batch Search of xx nucleotide sequences*

*Database:* select custom database (or alternatively, you can directly select from the list of NCBI databases)

*Program: blastn*

*Results: Hit Table*

*Retrieve: Matching region with annotations*

*Maximum hits: 1*

*MaxE-value: 1e-20*

![Blastn_2](./images/Blastn_2.png)

**Output**:  When the search is completed, a list with the BLASTn results will be compiled. Several parameters,  including Evalue, Query start/end, Hit start/end, can be checked to evaluate the expected frequency of occurrence of each alignment by chance and properties of the aligned regions. By clicking on each alignment, the aligned region between the query and the hit sequence is visualized. Note that the returned blast hit document is a summary and does not contain the full GenBank record for that sequence; to quickly download it click on “*Dowload Full Sequence(s)*”. Modify the BLASTn parameters accordingly, to get more hits or change the stringency of the BLASTn alignment.

![Blastn_3](./images/Blastn_3.png)

## 5. Running a Workflow
All steps can be combined in a convenient workflow. To build a workflow, go to:

*Workflow =>Manage Workflows =>New Workflow* and create one by combining pre-compiled modules or building step by step as wished. A Tutorial is available on the Geneious website [Creating-Worflows](https://www.geneious.com/tutorials/creating-workflows/).

The provided workflow combines all steps described before: reads will be trimmed, mapped to host mitochondrial/chloroplast genomes, mapped to host nuclear genome, *de novo* assembled; contigs will be generated and aligned to a reference database to generate a BLASTn results table.

To run the workflow, proceed as follows:
1. Import Workflow into Geneious through *Workflow=>Manage Workflows>=Import*
2. Click on *View/Edit* to open the workflow.
3. Click on the first “Align/Assemble” step, and define the reference (host chloroplast/mitochondrial genomes).
3. Click on the second “Align/Assemble” step, and define the reference (host nuclear genome).
4. Click on the BLAST step, and define the reference database.
5. Click OK to save the workflow.

![Running_1](./images/Running_1.png)

Go to the folder with the paired file(s) generated in step 2.2, select the file(s), click on *Workflows=>Run Workflow*=>Select the *Workflow to run*=>*OK*. During the various steps, Geneious provides information on the status of the running process in the pop-up window. The output will consist of six folders for each input file, named:
1. Trimmed
2. Minus_Host_CHL_MIT
3. Minus_Host_NuclearGenome
4. De_Novo
5. Contigs
6. BLASTn

You can now visualize in each folder the output results and the report file (when generated).
