# IPSP-CNR pipeline for Virus Detection in RNA-seq

## 0. Developers
[Michela Chiumenti](mailto:michela.chiumenti@ipsp.cnr.it) Institute for Sustainable Plant Protection, National Research Council ([IPSP – CNR](http://www.ipsp.cnr.it/?lang=en)), Italy

## 1. Introduction
This pipeline is used for Illumina total RNA and mRNA data, unpaired or paired reads. The pipeline can be run on a local Linux system or server, where the following software is installed and available in `$PATH`:

* [FASTX-Toolkit](http://hannonlab.cshl.edu/fastx_toolkit/)
*	[Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) (optional)
*	[SPAdes-3.9.0-Linux](https://cab.spbu.ru/software/spades/)
*	[BLAST+](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download)

The pipeline consists of three (or four) parts:
- a quality check/pre-processing part
- genome filtering if available (optional)
- *de novo* assembly and
- contig annotation using BLAST algorithm.

## 2. Quality check and data pre-processing
First, decompress your file using gunzip linux built-in function:
`gunzip <your_file.fastq.gz>`

If you have more files:
`gunzip *.fastq.gz`

In the following sections, the different pre-processing steps are explained.
### 2.1 QC
The FASTX-Toolkit is a collection of command line tools for Short-Reads FASTA/FASTQ files quality checks and pre-processing.

A quality report of the raw data can be done in different steps, first of all, it is necessary to run the `fastx_quality_stats` command using your fastq file as input:

>`fastx_quality_stats -i <your_file.fastq> -o <quality_stats_output_file> -Q33`

Note that the `–Q33` option is needed every time a fastq file is used as input.

The command `fastx_nucleotide_distribution_graph.sh` produces a barplot, indicating the distribution of the four nucleotides (plus Ns) along the positions read during the sequencing. The graphical output obtained gives an indication of possible read biases or contaminations. The input for this command is the output obtained using the `fastx_quality_stats`.

>`fastx_nucleotide_distribution_graph.sh -i <quality_stats_output_file> -o <output_nucleotide_distribution_graph.png> -t <graph_title>`

Using the `fastq_quality_boxplot_graph.sh` command, a boxplot graph is obtained which indicated the average Phred score for each position read. In the case of Illumina sequencing, this value should be higher than 20. The input to be used for this command is the output obtained using the `fastx_quality_stats`.

>`fastq_quality_boxplot_graph.sh -i <quality_stats_output_file -o <output_quality_boxplot.png> -t <graph_title>`

Based on the results obtained with the latter command, an additional filtering step, based on Phred score could be performed, with the `fastq_quality_filter` tool.

>`fastq_quality_filter -i <your_file.fastq> -q <quality_score_value> -o <your_file_quality_filtered.fastq> -Q33`

### 2.2 Remove adapter sequences
Next, you need to check your reads for the presence of 'Illumina' adapter sequences. Among the FASTX-Toolkit commands, there is the tool `fastx_clipper` which can be used to clip the sequences removing the Illumina adapter sequences. The following options can be used: `-i`: input file; `-o`: the output file name for the trimmed reads; `-a`: the adapter sequence.

>`fastx_clipper -i <your_file.fastq> -o <your_clipped_file.fastq> -a <ADAPTER SEQUENCE> -Q33`

### 2.3 Remove genome sequences (optional)
If a reference genome for the plant species you are working with is available, a genome/organelles filtering could be useful, to remove genomic reads that could interfere with the assembly process and reduce the elaboration time needed for the following steps. A further filtering step against chloroplast and mitochondrial sequences could be also run.

For this purpose, we use the software “bowtie2”, which finds matches to the sequence used as reference genome.

Once you downloaded your reference genome(s), before running the alignment itself, the reference sequence must be indexed though the `bowtie2-build` command, below an example is shown:

>`bowtie2-build your_genome.fasta your_indexed_output_genome`

 #### 2.3.1 Running the alignment
When running the alignment with bowtie2 it is important to keep the unaligned reads separately, indeed these will be later on used for the assembly step. To keep the unmapped reads your only need is to add the `--un` option to the command, which will be `--un-conc` in case you are working with paired end files. `–x`: introduce your reference genome, `-q`: your preprocessed fastq single read (SR) file, if you are working with paired end (PE) reads input file should be submitted with `-1` forward fastq and `-2` reverse fastq file. `--al` and `--al-conc` are used to obtain a file with all the mapped reads, working with SR and PE reads, respectively.

>*For example, SR experiment:*   `bowtie2 -x your_indexed_output_genome -q your_fastq.fastq --un your_unmapped_reads --al mapped_reads`


>*For example, PE experiment:* `bowtie2 -x your_indexed_output_genome -1 your_fastq_for.fastq -2 your_fastq_rev.fastq --un-conc your_unmapped_reads --al-conc mapped_reads`


To know all the usage options for bowtie2, type:

>`bowtie2 -help`

## 3. De novo assembly using SPAdes
Once your reads are ready, after all the pre-processing steps, it possible use them for *de novo* assembly. One powerful tool to assemble Illumina reads is SPAdes (Bankevich et al., 2012; Nurk et al., 2013).
The following options can be used: `--only-assembler`: runs assembly module only; `-k`: comma-separated list of k-mer sizes to be used, it requires odd values, listed in ascending order; `-o`: name of the output folder the program will create to store its output. Different kind of input files can be submitted: `--12`: file with interlaced forward and reverse paired-end reads; `-1`: file with forward reads; `-2`: file with reverse reads; `--merged`: file with merged paired reads; `-s`: file with unpaired reads.

>*For example, SR experiment:* `spades.py --only-assembler -k 21,51,71,91 -s <your_SR_file.fastq> -o <output_folder>`

>*For example, PE experiment:* `spades.py --only-assembler -k 21,51,71,91 -1 <your_fastq_for.fastq> -2 <your_fastq_for.fastq> -o <output_folder>`

For more information about the program and its options, see [CAB SPAdes 3.14.0 Manual](http://cab.spbu.ru/files/release3.14.0/manual.html).

References:

Bankevich, A., Nurk, S., Antipov, D., Gurevich, A. A., Dvorkin, M., Kulikov, A. S., ... & Pyshkin, A. V. (2012). SPAdes: a new genome assembly algorithm and its applications to single-cell sequencing. Journal of computational biology, 19(5), 455-477.

Nurk, S., Bankevich, A., Antipov, D., Gurevich, A. A., Korobeynikov, A., Lapidus, A., et al. (2013). Assembling single-cell genomes and mini-metagenomes from chimeric mda products. J. Comput. Biol. 20, 714–737. doi: 10.1089/cmb.2013.0084

## 4. BLAST annotation
Once your assembly is ready, it is possible to proceed with the annotation of the contigs. In the SPAdes output folder you will find a file named `contigs.fasta`, this will be used for the BLAST annotation. This step can be performed either online or locally. Usually the number of sequences contained in the contigs file obtained in SPAdes is too big to be submitted online in one go. Thus, downloading the BLAST+ executable is useful to speed up the annotation process. The executable files and their documentation are available at [BLAST executable files and documentation](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download).

### 4.1 Download and prepare reference database
To have a comprehensive idea of the viral content in your sample it is useful to run a `blastn` against the NCBI nt database and a `blastx` against a restricted database including only viruses and viroids.

Nt database is available on the NCBI ftp, from which it could easily be downloaded as follows:

>`wget ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nt.gz`

A viral specific database that could be used is the viral RefSeq available at [Index of /refseq/release/viral](https://ftp.ncbi.nlm.nih.gov/refseq/release/viral/), where it could be downloaded using `wget` linux built-in function as shown above. This is an example of viral database, other customised databases can be obtained using a fasta file containing the sequences of interest.

Once retrieved the reference(s), and before using it for annotation, it is necessary to index it using the `BLAST+` function `makeblastdb`. This function needs `–in`: input database; `-input_type`: specify the file format of the input file; `-dbtype`: indicates if the input file contains nucleotide or protein sequences; `-title`: option used to specify the title you want to give to the database obtained from this input.

>*For example:*
`makeblastdb -in nt -input_type fasta -dbtype nucl -title nt`

To know all the usage options for `makeblastdb`, type:

>`makeblastdb -help`

### 4.2 Running blast to annotate your contigs
The following options can be used: `-db`: indicate the reference database to be used for the annotation; `-query`: input file, fasta or multifasta to annotate; `-out`: name of the output file; `-max_target_seqs`: maximum number of aligned sequences to keep (default = 500); `-outfmt`: integer number ranging from 0 to 18, indicating the alignment view option (i.e. most commonly used 5 = BLAST XML, 7 = Tabular with comment lines, 10 = Comma-separated values); `-evalue`: expectation value (E) threshold for saving hits (default=10).

>*For example, BLASTn command:*
`blastn -db nt -query contigs.fasta -out <blastn_output> -max_target_seqs 10 -outfmt 5 -evalue 0.0001`

>*For example, BLASTx command:*
`blastx -db Protein_ViralRefSeq.faa –query contigs.fasta -out <blastx_output.xml> -max_target_seqs 10 -outfmt 5 -evalue 0.01`

To know all the usage options for blast, type:

>*For example:*
`blastn -help`
